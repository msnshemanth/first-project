({
    myAction : function(component, event, helper) 
       {
           component.set('v.columns',[{ label: 'FirstName', fieldName: 'FirstName' },
                                      { label: 'LastName', fieldName: 'LastName' }]);

           var ConList = component.get("c.getRelatedList");
           ConList.setParams
           ({
               recordId: component.get("v.recordId")
           });
           
           ConList.setCallback(this, function(data) 
                              {
                                  component.set("v.ContactList", data.getReturnValue());
                                  //component.set("v.conLst",data.getReturnValue());
                                  console.log('Check : '+data.getReturnValue());
                              });
           $A.enqueueAction(ConList);
    }
   })
