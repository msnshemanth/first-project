({
    handlingSearchEvent : function(component, event, helper) {

        //event.getParam is getting attribute value from event "contacts" is attribute name in the EVENT
        var contactLst = event.getParam("contacts");
        component.set("v.appContacts",contactLst);
        component.set("v.showTable",true);
    }
})