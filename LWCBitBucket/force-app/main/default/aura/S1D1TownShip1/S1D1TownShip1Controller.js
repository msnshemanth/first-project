({
    forwardLPComplaint : function(component, event, helper) {
        //fire the component event, below are the steps involved
        //1. get a reference to our component event
        var LPCEvent = component.getEvent("LowPriorityComplaintFromTownShip1");
        //2.set the attribute value
        LPCEvent.setParams({"complaintMessage":"TownShip 2 is not behaving properly..."});
        //3.fire the event
        LPCEvent.fire();
    },
    forwardMPComplaint : function(component,event,helper){
        var MPCEvent = component.getEvent("MediumPriorityComplaintFromTownShip1");
        MPCEvent.setParams({"complaintMessage":"TownShip 2 in dumping waste in township 1"});
        MPCEvent.fire();
    },
    forwardHPComplaint : function(component,event,helper){
        var HPCEvent = component.getEvent("HighPriorityComplaintFromTownShip1");
        HPCEvent.setParams({"complaintMessage":"TownShip 2 is fucked up now."});
        HPCEvent.fire();
    }
})