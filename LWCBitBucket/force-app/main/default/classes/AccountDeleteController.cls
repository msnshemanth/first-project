public class  AccountDeleteController{
    @AuraEnabled(cacheable=true)
    public static List<Account> displayAccounts(){
        return [select Id,Name,Phone from Account];
    }
    @AuraEnabled
    public static List<Account> delAccRecords(List<String> selecRecords){
        List<Account> returnlist=new List<Account>();
        for(Account acc:[select Id,Name,Phone from Account where Id in:selecRecords]){
            returnlist.add(acc);

        }
        if(returnlist.size()>0){
            try{
                delete returnlist;
            }
            catch(Exception  e){
                System.debug('unable to delete the record due to'+e.getMessage());
            }
        }
        return displayAccounts();
        
    }
    
}