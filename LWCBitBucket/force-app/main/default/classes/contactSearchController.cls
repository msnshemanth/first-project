public with sharing class contactSearchController {
    @AuraEnabled
    public static List<Contact> getContactList(string searchTerm){

        string searchCon = '%' +searchTerm +'%';
        list<Contact> contactsLst = [SELECT Id, name , email FROM contact WHERE Name LIKE :searchCon];
        return contactsLst;
    }
}