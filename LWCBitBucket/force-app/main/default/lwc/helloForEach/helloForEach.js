import { LightningElement, track} from 'lwc';

export default class HelloForEach extends LightningElement {
  @track contacts =[
    {
      id:1,
      Name:'MSNS Hemanth',
      Title:'Sr.Salesforce Developer'
    },
    {
      id:2,
      Name:'MSNS',
      Title:'Salesforce Developer'
    },
    {
      id:3,
      Name:'Hemanth',
      Title:'Sr. Developer'
    },
    {
      id:4,
      Name:'Hem',
      Title:'Developer'
    },
  ];
}