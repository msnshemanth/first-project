import { LightningElement,wire,api } from 'lwc';
import  displayAccounts from '@salesforce/apex/AccountDeleteController.displayAccounts';
import delAccRecords from '@salesforce/apex/AccountDeleteController.delAccRecords';
import { refreshApex } from '@salesforce/apex';
export default class BasicDemo extends LightningElement {
    @api  columns =[
        { label: 'Name', fieldName: 'Name' },
        { label: 'Phone', fieldName: 'Phone'}
    ];
    @wire(displayAccounts) accounts;
    @api selectedAccountlist=[];
    @api errorMessage;
    getSelectedId(event){
        const selectedRows = event.detail.selectedRows;
        console.log('selectedRecordID'+JSON.stringify(selectedRows));
        this.selectedAccountlist=[];
        for (let i = 0; i<selectedRows.length; i++){
            this.selectedAccountlist.push(selectedRows[i].Id);
        }
    }
    handleDelete(){
        delAccRecords({selecRecords: this.selectedAccountlist })
        .then(()=>{
            this.template.querySelector('lightning-datatable').selectedRows=[];
            return refreshApex(this.accounts);
        })
        .catch((error)=>{
            this.errorMessage=error;
            console.log('unable to delete the record due to'+JSON.stringify(this.errorMessage));
        });

    }
  }